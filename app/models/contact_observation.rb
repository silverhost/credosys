class ContactObservation < ActiveRecord::Base
  belongs_to :contact

  validates_presence_of :comment
end
