class Campaign < ActiveRecord::Base
  belongs_to :campaign_type
  has_many :contact_activities
end
