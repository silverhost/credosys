class Contact < ActiveRecord::Base
  has_many :contact_observations, dependent: :destroy
  has_many :contact_activities, dependent: :destroy
  has_one :contact_information, dependent: :destroy
  accepts_nested_attributes_for :contact_observations, reject_if: proc { |attributes| attributes['comment'].blank? }
  accepts_nested_attributes_for :contact_information

  scope :group_by_month,   -> { group("date_trunc('month', created_at) ") }


  def self.to_csv(options = {})
    CSV.generate(options) do |csv|
      csv << column_names
      all.each do |contact|
        csv << contact.attributes.values_at(*column_names)
      end
    end
  end

  scope :this_month, -> { where(created_at: Time.now.beginning_of_month..Time.now.end_of_month) }

end
