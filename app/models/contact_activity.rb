class ContactActivity < ActiveRecord::Base
  belongs_to :contact, counter_cache: true
  belongs_to :campaign
  belongs_to :activity
end
