json.array!(@campaign_types) do |campaign_type|
  json.extract! campaign_type, :id, :description
  json.url campaign_type_url(campaign_type, format: :json)
end
