json.array!(@contact_activities) do |contact_activity|
  json.extract! contact_activity, :id, :contact_id, :campaign_id, :activity_id, :description
  json.url contact_activity_url(contact_activity, format: :json)
end
