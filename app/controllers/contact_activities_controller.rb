class ContactActivitiesController < ApplicationController
  before_action :authenticate_user!
  before_action :set_contact_activity, only: [:show, :edit, :update, :destroy]

  # GET /contact_activities/1
  # GET /contact_activities/1.json
  def show
  end

  # GET /contact_activities/new
  def new
    @contact_activity = ContactActivity.new
  end

  # GET /contact_activities/1/edit
  def edit
  end

  # POST /contact_activities
  # POST /contact_activities.json
  def create
    @contact_activity = ContactActivity.new(contact_activity_params)

    respond_to do |format|
      if @contact_activity.save
        format.html { redirect_to @contact_activity, notice: 'Contact activity was successfully created.' }
        format.json { render :show, status: :created, location: @contact_activity }
      else
        format.html { render :new }
        format.json { render json: @contact_activity.errors, status: :unprocessable_entity }
      end
    end
  end

  # PATCH/PUT /contact_activities/1
  # PATCH/PUT /contact_activities/1.json
  def update
    respond_to do |format|
      if @contact_activity.update(contact_activity_params)
        format.html { redirect_to @contact_activity, notice: 'Contact activity was successfully updated.' }
        format.json { render :show, status: :ok, location: @contact_activity }
      else
        format.html { render :edit }
        format.json { render json: @contact_activity.errors, status: :unprocessable_entity }
      end
    end
  end

  # DELETE /contact_activities/1
  # DELETE /contact_activities/1.json
  def destroy
    @contact_activity.destroy
    respond_to do |format|
      format.html { redirect_to contact_activities_url, notice: 'Contact activity was successfully destroyed.' }
      format.json { head :no_content }
    end
  end

  private
    # Use callbacks to share common setup or constraints between actions.
    def set_contact_activity
      #@contact_activity = ContactActivity.find(params[:id])
      @contact = Contact.find(params[:id])
      @contact_activities = ContactActivity.where(contact_id: params[:id]).order(created_at: :desc)
    end

    # Never trust parameters from the scary internet, only allow the white list through.
    def contact_activity_params
      params.require(:contact_activity).permit(:contact_id, :campaign_id, :activity_id, :description)
    end
end
