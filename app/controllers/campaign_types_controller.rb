class CampaignTypesController < ApplicationController
  before_action :authenticate_user!
  before_action :set_campaign_type, only: [:show, :edit, :update, :destroy]

  # GET /campaign_types
  # GET /campaign_types.json
  def index
    @campaign_types = CampaignType.all
  end

  # GET /campaign_types/1
  # GET /campaign_types/1.json
  def show
  end

  # GET /campaign_types/new
  def new
    @campaign_type = CampaignType.new
  end

  # GET /campaign_types/1/edit
  def edit
  end

  # POST /campaign_types
  # POST /campaign_types.json
  def create
    @campaign_type = CampaignType.new(campaign_type_params)

    respond_to do |format|
      if @campaign_type.save
        format.html { redirect_to @campaign_type, notice: 'Campaign type was successfully created.' }
        format.json { render :show, status: :created, location: @campaign_type }
      else
        format.html { render :new }
        format.json { render json: @campaign_type.errors, status: :unprocessable_entity }
      end
    end
  end
  
  # PATCH/PUT /campaign_types/1
  # PATCH/PUT /campaign_types/1.json
  def update
    respond_to do |format|
      if @campaign_type.update(campaign_type_params)
        format.html { redirect_to @campaign_type, notice: 'Campaign type was successfully updated.' }
        format.json { render :show, status: :ok, location: @campaign_type }
      else
        format.html { render :edit }
        format.json { render json: @campaign_type.errors, status: :unprocessable_entity }
      end
    end
  end

  # DELETE /campaign_types/1
  # DELETE /campaign_types/1.json
  def destroy
    @campaign_type.destroy
    respond_to do |format|
      format.html { redirect_to campaign_types_url, notice: 'Campaign type was successfully destroyed.' }
      format.json { head :no_content }
    end
  end

  private
    # Use callbacks to share common setup or constraints between actions.
    def set_campaign_type
      @campaign_type = CampaignType.find(params[:id])
    end

    # Never trust parameters from the scary internet, only allow the white list through.
    def campaign_type_params
      params.require(:campaign_type).permit(:description)
    end
end
