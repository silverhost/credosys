class ContactsController < ApplicationController
  before_action :authenticate_user!
  before_action :set_contact, only: [:show, :show_activities, :edit, :update, :destroy]

  # GET /contacts
  # GET /contacts.json
  def index
    @contacts = Contact.all
    respond_to do |format|
      format.html
      format.json { render json: ContactDatatable.new(view_context) }
      format.csv { send_data @contacts.to_csv }
      format.xls # { send_data @products.to_csv(col_sep: "\t")
    end
  end

  # GET /contacts/1
  # GET /contacts/1.json
  def show
    @campaign = Campaign.find_by name: @contact.source

  end

  # GET /contacts/new
  def new
    @contact = Contact.new
    @contact.contact_observations.build
    @contact.build_contact_information
  end

  # GET /contacts/1/edit
  def edit
    @contact.contact_observations.build
    
  end

  # POST /contacts
  # POST /contacts.json
  def create
    @contact = Contact.new(contact_params)

    respond_to do |format|
      if @contact.save
        format.html { redirect_to @contact, notice: 'Contact was successfully created.' }
        format.json { render :show, status: :created, location: @contact }
      else
        format.html { render :new }
        format.json { render json: @contact.errors, status: :unprocessable_entity }
      end
    end
  end

  # PATCH/PUT /contacts/1
  # PATCH/PUT /contacts/1.json
  def update
    respond_to do |format|
      if @contact.update(contact_params)
        format.html { redirect_to @contact, notice: 'Contact was successfully updated.' }
        format.json { render :show, status: :ok, location: @contact }
      else
        format.html { render :edit }
        format.json { render json: @contact.errors, status: :unprocessable_entity }
      end
    end
  end

  # DELETE /contacts/1
  # DELETE /contacts/1.json
  def destroy
    @contact.destroy
    respond_to do |format|
      format.html { redirect_to contacts_url, notice: 'Contact was successfully destroyed.' }
      format.json { head :no_content }
    end
  end

  private
    # Use callbacks to share common setup or constraints between actions.
    def set_contact
      @contact = Contact.find(params[:id])
    end

    # Never trust parameters from the scary internet, only allow the white list through.
    def contact_params
      params.require(:contact).permit(:first_name, :last_name, :email, :source, :donor, contact_information_attributes: [:rut, :birthdate, :phone, :address, :commune, :region], contact_observations_attributes: [:comment, :id])
    end
end
