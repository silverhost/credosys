class PagesController < ApplicationController
  before_action :authenticate_user!

  def dashboard
    @totalContacts = Contact.count(:all)
    @totalCampaigns = Campaign.count(:all)
    @activeContacts = Contact.where("contacts.activities_count >= 2").count
    @onceContacts = Contact.where("contacts.activities_count = 1").count
    @inactiveContacts = Contact.where("contacts.activities_count = 0").count
    @averageActivity = Contact.average(:activities_count).round(2)

    @contactsOfTheMonth = Contact.this_month.count

    @contactTop = Contact.where("contacts.activities_count").order('activities_count DESC').limit(15)

    # Selector por fecha de actividades



    @contact_months = Contact.order(:created_at).group_by { |m| m.created_at.beginning_of_month }

    @dataContact = []
    @contact_months.each do |m|
       @dataContact << ["Date.UTC(#{m[0].strftime("%Y, %m, %d")})", m[1].count]

     end


  end

  def ranking
    @quantity = params['quantity']
    if @quantity == nil
      @quantity = 100
    end
    @top = Contact.all.order('activities_count DESC').limit(@quantity)
  end
end
