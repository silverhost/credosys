# Place all the behaviors and hooks related to the matching controller here.
# All this logic will automatically be available in application.js.
# You can use CoffeeScript in this file: http://coffeescript.org/
jQuery ->
  $('#list_contacts').dataTable
    sPaginationType: 'full_numbers'
    bProcessing: true
    bServerSide: true
    sAjaxSource: $('#list_contacts').data('source')
    columns: [
      null,
      null,
      null,
      null,
      null,
      null,
      { 'orderable': false },
      { 'orderable': false },
      { 'orderable': false },

    ]
    order: [[ 0, "desc" ]]
    language: {
      url: "//cdn.datatables.net/plug-ins/1.10.15/i18n/Spanish.json"
    }
