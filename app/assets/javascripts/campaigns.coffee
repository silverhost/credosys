# Place all the behaviors and hooks related to the matching controller here.
# All this logic will automatically be available in application.js.
# You can use CoffeeScript in this file: http://coffeescript.org/
jQuery ->
  $('#list_campaigns').dataTable
    sPaginationType: 'full_numbers'
    bProcessing: true
    bServerSide: true
    sAjaxSource: $('#list_campaigns').data('source')
    columns: [
      null,
      null,
      null,
      { 'orderable': false },
      { 'orderable': false },
      { 'orderable': false }
    ]
