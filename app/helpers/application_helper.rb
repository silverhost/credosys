module ApplicationHelper
end


def nav_link(link_text, link_path, icon = '')
  class_name = current_page?(link_path) ? 'active' : ''

  content_tag(:li, :class => class_name) do
    link_to link_path do
      ('<i class="' + icon + '"></i> ' + link_text).html_safe
    end
  end
end

def table_link(link_text, link_path, icon = '')
  class_name = current_page?(link_path) ? 'link-table' : ''

  content_tag(:td, :class => class_name) do
    link_to link_path do
      (link_text + '<i class="' + icon + '"></i> ').html_safe
    end
  end
end

def is_active?(link_path)
  if current_page?(link_path)
    "active"
  else
    ""
  end
end