class ContactDatatable
  delegate :params, :fa_icon, :link_to, :contact_path, :contact_activity_path, :edit_contact_path, to: :@view

  def initialize(view)
    @view = view
  end

  def as_json(options = {})
    {
      sEcho: params[:sEcho].to_i,
      iTotalRecords: Contact.count,
      iTotalDisplayRecords: contacts.total_count,
      aaData: data
    }
  end

private

  def data
    contacts.map do |contact|
      [
        contact.id,
        contact.first_name,
        contact.last_name,
        contact.email,
        contact.activities_count,
        contact.last_activity_at,
        link_to(fa_icon('eye'), contact_path(contact) ),
        link_to(fa_icon('list'), contact_activity_path(contact) ),
        [
         link_to(fa_icon('pencil'), edit_contact_path(contact) ) ,
         link_to(fa_icon('trash'), contact_path(contact), method: :delete, data: { confirm: 'Are you sure?' })
        ]
      ]
    end
  end

  def contacts
    @contacts ||= fetch_contacts
  end

  def fetch_contacts
    contacts = Contact.order("#{sort_column} #{sort_direction}")
    contacts = contacts.page(page).per(per_page)
    if params[:sSearch].present?
      contacts = contacts.where("first_name like :search or last_name like :search or email like :search or activities_count = :search", search: "%#{params[:sSearch]}%")
    end
    contacts
  end

  def page
    params[:iDisplayStart].to_i / per_page + 1
  end

  def per_page
    params[:iDisplayLength].to_i > 0 ? params[:iDisplayLength].to_i : 10
  end

  def sort_column
    columns = %w[id first_name last_name email activities_count last_activity_at]
    columns[params[:iSortCol_0].to_i]
  end

  def sort_direction
    params[:sSortDir_0] == 'desc' ? 'desc' : 'asc'
  end
end
