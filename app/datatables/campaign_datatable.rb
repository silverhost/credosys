class CampaignDatatable
  delegate :params, :fa_icon, :link_to, :campaign_path, :edit_campaign_path, to: :@view

  def initialize(view)
    @view = view
  end

  def as_json(options = {})
    {
      sEcho: params[:sEcho].to_i,
      iTotalRecords: Campaign.count,
      iTotalDisplayRecords: campaigns.total_count,
      aaData: data
    }
  end

private

  def data
    campaigns.map do |campaign|
      [
        campaign.name,
        campaign.campaign_type.description,
        campaign.created_at,
        link_to(fa_icon('eye'), campaign_path(campaign) ),
        link_to(fa_icon('pencil'), edit_campaign_path(campaign) ),
        link_to(fa_icon('trash'), campaign_path(campaign), method: :delete, data: { confirm: 'Are you sure?' })
      ]
    end
  end

  def campaigns
    @campaigns ||= fetch_campaigns
  end

  def fetch_campaigns
    campaigns = Campaign.order("#{sort_column} #{sort_direction}")
    campaigns = campaigns.page(page).per(per_page)
    if params[:sSearch].present?
      campaigns = campaigns.where("name like :search", search: "%#{params[:sSearch]}%")
    end
    campaigns
  end

  def page
    params[:iDisplayStart].to_i / per_page + 1
  end

  def per_page
    params[:iDisplayLength].to_i > 0 ? params[:iDisplayLength].to_i : 10
  end

  def sort_column
    columns = %w[name type created_at]
    columns[params[:iSortCol_0].to_i]
  end

  def sort_direction
    params[:sSortDir_0] == 'desc' ? 'desc' : 'asc'
  end
end
