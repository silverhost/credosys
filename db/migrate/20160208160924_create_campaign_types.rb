class CreateCampaignTypes < ActiveRecord::Migration
  def change
    create_table :campaign_types do |t|
      t.string :description

      t.timestamps null: false
    end
  end
end
