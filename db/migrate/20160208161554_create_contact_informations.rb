class CreateContactInformations < ActiveRecord::Migration
  def change
    create_table :contact_informations do |t|
      t.string :phone
      t.string :rut
      t.date :birthdate
      t.string :address
      t.string :region
      t.string :commune
      t.references :contact, index: true, foreign_key: true

      t.timestamps null: false
    end
  end
end
