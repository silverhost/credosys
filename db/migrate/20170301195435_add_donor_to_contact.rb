class AddDonorToContact < ActiveRecord::Migration
  def change
    add_column :contacts, :donor, :string
  end
end
