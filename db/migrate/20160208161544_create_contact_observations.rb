class CreateContactObservations < ActiveRecord::Migration
  def change
    create_table :contact_observations do |t|
      t.text :comment
      t.references :contact, index: true, foreign_key: true

      t.timestamps null: false
    end
  end
end
