class AddActivityCounterCacheToContact < ActiveRecord::Migration
  def up
      add_column :contacts, :activities_count, :integer
      Contact.reset_column_information
      Contact.find_each do |contact|
        contact.update_attribute(:activities_count, contact.contact_activities.length)
      end
    end

    def down
      remove_column :contacts, :activities_count
    end
end
