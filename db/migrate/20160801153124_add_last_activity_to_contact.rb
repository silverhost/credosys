class AddLastActivityToContact < ActiveRecord::Migration
  def change
    add_column :contacts, :last_activity_at, :datetime
  end
end
