class CreateContactActivities < ActiveRecord::Migration
  def change
    create_table :contact_activities do |t|
      t.references :contact, index: true, foreign_key: true
      t.references :campaign, index: true, foreign_key: true
      t.references :activity, index: true, foreign_key: true
      t.text :description

      t.timestamps null: false
    end
  end
end
