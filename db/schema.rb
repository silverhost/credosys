# encoding: UTF-8
# This file is auto-generated from the current state of the database. Instead
# of editing this file, please use the migrations feature of Active Record to
# incrementally modify your database, and then regenerate this schema definition.
#
# Note that this schema.rb definition is the authoritative source for your
# database schema. If you need to create the application database on another
# system, you should be using db:schema:load, not running all the migrations
# from scratch. The latter is a flawed and unsustainable approach (the more migrations
# you'll amass, the slower it'll run and the greater likelihood for issues).
#
# It's strongly recommended that you check this file into your version control system.

ActiveRecord::Schema.define(version: 20170301195435) do

  create_table "activities", force: :cascade do |t|
    t.string   "description", limit: 255
    t.datetime "created_at",              null: false
    t.datetime "updated_at",              null: false
  end

  create_table "campaign_types", force: :cascade do |t|
    t.string   "description", limit: 255
    t.datetime "created_at",              null: false
    t.datetime "updated_at",              null: false
  end

  create_table "campaigns", force: :cascade do |t|
    t.string   "name",             limit: 255
    t.integer  "campaign_type_id", limit: 4
    t.datetime "created_at",                   null: false
    t.datetime "updated_at",                   null: false
  end

  add_index "campaigns", ["campaign_type_id"], name: "index_campaigns_on_campaign_type_id", using: :btree

  create_table "contact_activities", force: :cascade do |t|
    t.integer  "contact_id",  limit: 4
    t.integer  "campaign_id", limit: 4
    t.integer  "activity_id", limit: 4
    t.text     "description", limit: 65535
    t.datetime "created_at",                null: false
    t.datetime "updated_at",                null: false
  end

  add_index "contact_activities", ["activity_id"], name: "index_contact_activities_on_activity_id", using: :btree
  add_index "contact_activities", ["campaign_id"], name: "index_contact_activities_on_campaign_id", using: :btree
  add_index "contact_activities", ["contact_id"], name: "index_contact_activities_on_contact_id", using: :btree

  create_table "contact_informations", force: :cascade do |t|
    t.string   "phone",      limit: 255
    t.string   "rut",        limit: 255
    t.date     "birthdate"
    t.string   "address",    limit: 255
    t.string   "region",     limit: 255
    t.string   "commune",    limit: 255
    t.integer  "contact_id", limit: 4
    t.datetime "created_at",             null: false
    t.datetime "updated_at",             null: false
  end

  add_index "contact_informations", ["contact_id"], name: "index_contact_informations_on_contact_id", using: :btree

  create_table "contact_observations", force: :cascade do |t|
    t.text     "comment",    limit: 65535
    t.integer  "contact_id", limit: 4
    t.datetime "created_at",               null: false
    t.datetime "updated_at",               null: false
  end

  add_index "contact_observations", ["contact_id"], name: "index_contact_observations_on_contact_id", using: :btree

  create_table "contacts", force: :cascade do |t|
    t.string   "first_name",       limit: 255
    t.string   "last_name",        limit: 255
    t.string   "email",            limit: 255
    t.string   "source",           limit: 255
    t.datetime "created_at",                   null: false
    t.datetime "updated_at",                   null: false
    t.datetime "last_activity_at"
    t.integer  "activities_count", limit: 4
    t.string   "donor",            limit: 255
  end

  create_table "users", force: :cascade do |t|
    t.string   "email",                  limit: 255, default: "", null: false
    t.string   "encrypted_password",     limit: 255, default: "", null: false
    t.string   "reset_password_token",   limit: 255
    t.datetime "reset_password_sent_at"
    t.datetime "remember_created_at"
    t.integer  "sign_in_count",          limit: 4,   default: 0,  null: false
    t.datetime "current_sign_in_at"
    t.datetime "last_sign_in_at"
    t.string   "current_sign_in_ip",     limit: 255
    t.string   "last_sign_in_ip",        limit: 255
    t.datetime "created_at",                                      null: false
    t.datetime "updated_at",                                      null: false
  end

  add_index "users", ["email"], name: "index_users_on_email", unique: true, using: :btree
  add_index "users", ["reset_password_token"], name: "index_users_on_reset_password_token", unique: true, using: :btree

  add_foreign_key "campaigns", "campaign_types"
  add_foreign_key "contact_activities", "activities"
  add_foreign_key "contact_activities", "campaigns"
  add_foreign_key "contact_activities", "contacts"
  add_foreign_key "contact_informations", "contacts"
  add_foreign_key "contact_observations", "contacts"
end
