require 'test_helper'

class ContactActivitiesControllerTest < ActionController::TestCase
  setup do
    @contact_activity = contact_activities(:one)
  end

  test "should get index" do
    get :index
    assert_response :success
    assert_not_nil assigns(:contact_activities)
  end

  test "should get new" do
    get :new
    assert_response :success
  end

  test "should create contact_activity" do
    assert_difference('ContactActivity.count') do
      post :create, contact_activity: { activity_id: @contact_activity.activity_id, campaign_id: @contact_activity.campaign_id, contact_id: @contact_activity.contact_id, description: @contact_activity.description }
    end

    assert_redirected_to contact_activity_path(assigns(:contact_activity))
  end

  test "should show contact_activity" do
    get :show, id: @contact_activity
    assert_response :success
  end

  test "should get edit" do
    get :edit, id: @contact_activity
    assert_response :success
  end

  test "should update contact_activity" do
    patch :update, id: @contact_activity, contact_activity: { activity_id: @contact_activity.activity_id, campaign_id: @contact_activity.campaign_id, contact_id: @contact_activity.contact_id, description: @contact_activity.description }
    assert_redirected_to contact_activity_path(assigns(:contact_activity))
  end

  test "should destroy contact_activity" do
    assert_difference('ContactActivity.count', -1) do
      delete :destroy, id: @contact_activity
    end

    assert_redirected_to contact_activities_path
  end
end
